# Problem 120
#
# Square remainders
#
# Let r be the remainder when (a−1)n + (a+1)n is divided by a2.
# For example, if a = 7 and n = 3, then r = 42: 63 + 83 = 728 ≡ 42 mod 49. And as n varies, so too will r, but for a = 7 it turns out that rmax = 42.
# For 3 ≤ a ≤ 1000, find <span style="font-family:times new roman;font-size:13pt;">∑</span> rmax.
#
# https://projecteuler.net/problem=120
