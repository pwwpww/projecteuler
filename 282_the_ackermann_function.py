# Problem 282
#
# The Ackermann function
#
# 
# For non-negative integers m, n, the Ackermann function A(m, n) is defined as follows:
# 
# 
# For example A(1, 0) = 2, A(2, 2) = 7 and A(3, 4) = 125.
# 
# 
# Find <img src="project/images/p282formula3.gif" style="vertical-align:middle"/>A(n, n) and give your answer mod 148.
#
# https://projecteuler.net/problem=282
