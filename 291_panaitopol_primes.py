# Problem 291
#
# Panaitopol Primes
#
# 
# A prime number p is called a Panaitopol prime if <img src="project/images/p291_formula.gif" style="vertical-align:middle"/> for some positive integers x and y.
# 
# 
# Find how many Panaitopol primes are less than 5×1015.
# 
#
# https://projecteuler.net/problem=291
