# Problem 1
#
# Multiples of 3 and 5
#
# If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
# Find the sum of all the multiples of 3 or 5 below 1000.
#
# https://projecteuler.net/problem=1

import numpy as np

BELOW_NUMBER = 1000

total = [0]
three = [3]
five = [5]

while True:
    three_new = three[0] + three[-1]
    print(three_new)
    if three_new < BELOW_NUMBER:
        three.append(three_new)

    five_new = five[0] + five[-1]
    print(five_new)
    if five_new < BELOW_NUMBER:
        five.append(five_new)

    if three_new >= BELOW_NUMBER and five_new >= BELOW_NUMBER:
        break

total.extend(three)
total.extend(five)
total = np.unique(np.asarray(total))

print(total)
print("Sum of all the multiples of 3 or 5 below 1000: %d" % total.sum())