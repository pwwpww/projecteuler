# Problem 4
#
# Largest palindrome product
#
# A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
# Find the largest palindrome made from the product of two 3-digit numbers.
#
# https://projecteuler.net/problem=4

def ispalindrome(n):
    chkstr = str(n)
    chk_a = chkstr[:int(len(chkstr)/2)]
    #print(chk_a)
    chk_b = chkstr[:int(len(chkstr)/2-1):-1]
    #print(chk_b)
    return chk_a == chk_b

biggest_product = 0

for i in range(999,900,-1):
    for j in range(999,900,-1):
        product = i * j
        if ispalindrome(product):
            if biggest_product < product:
                biggest_product = product
                print(biggest_product)


                def fib(n):
    if n <= 0:
        return []
    if n == 1:
        return [0]
    result = [0, 1, 1]
    while (result[-1] + result[-2]) < n:
        result.append(result[-1] + result[-2])
    return result


print(fib())