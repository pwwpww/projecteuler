# Problem 508
#
# Integers in base i-1
#
# Consider the Gaussian integer i-1. A base i-1 representation of a Gaussian integer a+bi is a finite sequence of digits dn-1dn-2...d1d0 such that:
# Here are base i-1 representations of a few Gaussian integers:
# 11+24i â 111010110001101
# 24-11i â 110010110011
# 8+0i â 111000000
# -5+0i â 11001101
# 0+0i â 0
#
# https://projecteuler.net/problem=508
