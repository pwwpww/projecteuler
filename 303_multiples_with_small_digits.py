# Problem 303
#
# Multiples with small digits
#
# 
# For a positive integer n, define f(n) as the least positive multiple of n that, written in base 10, uses only digits ≤ 2.
# Thus f(2)=2, f(3)=12, f(7)=21, f(42)=210, f(89)=1121222.
# Also, <img src="project/images/p303_formula100.gif" style="vertical-align:middle"/>.
# 
# Find <img src="project/images/p303_formula10000.gif" style="vertical-align:middle"/>.
# 
#
# https://projecteuler.net/problem=303
