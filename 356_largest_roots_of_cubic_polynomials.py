# Problem 356
#
# Largest roots of cubic polynomials
#
# 
# Let an be the largest real root of a polynomial g(x) = x3 - 2n·x2 + n.
# For example, a2 = 3.86619826...
# 
# Find the last eight digits of<img src="project/images/p356_cubicpoly1.gif" style="vertical-align:middle"/>.
# 
<u>Note</u>: <img src="project/images/p356_cubicpoly2.gif" style="vertical-align:middle"/> represents the floor function.
#
# https://projecteuler.net/problem=356
