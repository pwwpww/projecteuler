# Problem 372
#
# Pencils of rays
#
# 
# Let R(M, N) be the number of lattice points (x, y) which satisfy M&lt;x≤N, M&lt;y≤N and <img src="project/images/p372_pencilray1.jpg" style="vertical-align:middle"/> is odd.
# We can verify that R(0, 100) = 3019 and R(100, 10000) = 29750422.
# Find R(2·106, 109).
# 
# 
<u>Note</u>: <img src="project/images/p372_pencilray2.gif" style="vertical-align:middle"/> represents the floor function.
#
# https://projecteuler.net/problem=372
