import requests, re, os
from bs4 import BeautifulSoup


def get_valid_filename(s):
    """ taken from Django
    Returns the given string converted to a string that can be used for a clean
    filename. Specifically, leading and trailing spaces are removed; other
    spaces are converted to underscores; and anything that is not a unicode
    alphanumeric, dash, underscore, or dot, is removed.
    >>> get_valid_filename("john's portrait in 2004.jpg")
    'johns_portrait_in_2004.jpg'
    """
    s = s.strip().replace(' ', '_')
    return re.sub(r'(?u)[^-\w.]', '', s)


def remove_tags(strip):
    strip = strip.replace("<p>","")
    strip = strip.replace("</p>","")
    strip = strip.replace("<i>","")
    strip = strip.replace("</i>","")
    strip = strip.replace("<b>","")
    strip = strip.replace("</b>","")
    strip = strip.replace("<sup>","")
    strip = strip.replace("</sup>","")
    strip = strip.replace("<sub>","")
    strip = strip.replace("</sub>","")
    strip = strip.replace('<br/>',"")
    strip = strip.replace("<h2>","")
    strip = strip.replace("</h2>","")
    strip = strip.replace("<var>","")
    strip = strip.replace("</var>","")
    strip = strip.replace("\r\n", os.linesep + "# ")

    return strip


START_PROBLEM_NUMBER = 521
END_PROBLEM_NUMBER = 667

'''
Outputs a file in a form:

(in file 1_multiples_of_3_and_5.py)
# Problem 1
#
# Multiples of 3 and 5
#
# If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
# Find the sum of all the multiples of 3 or 5 below 1000.

'''

for euler_number in range(START_PROBLEM_NUMBER, END_PROBLEM_NUMBER + 1):
    soup = BeautifulSoup(requests.get('https://projecteuler.net/problem=' + str(euler_number)).text)

    # array holding all the lines
    to_write = []

    # problem number
    to_write.extend(["# " + "Problem " + str(euler_number), "#"])

    # problem name
    problem_name = remove_tags(str(soup.h2))
    to_write.extend(["# " + problem_name, "#"])

    # problem description

    for description in soup.find_all('p'):
        strip = str(description)
        if "<dfn title=" in strip:
            attribute = strip[strip.find("<dfn title=\"") + 12:strip.find("\">")]
            strip = strip.replace("<dfn title=\"", "")
            strip = strip.replace("\">", "")
            strip = strip.replace(attribute, "")
            strip = strip.replace("</dfn>", " (" + attribute + ")")
        # print(strip)
        strip = remove_tags(strip)
        # print(strip)
        to_write.append("# " + strip)

    # website
    to_write.extend(["#", "# " + 'https://projecteuler.net/problem=' + str(euler_number)])

    file_name = get_valid_filename((str(euler_number) + " " + to_write[2][2:]).lower() + ".py")

    with open(file_name, mode='wt', encoding='utf-8') as file:
        file.write('\n'.join(to_write))
        file.write('\n')

    print(soup.title.string[:-16] + " file has been created")