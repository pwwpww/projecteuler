# Problem 3
#
# Largest prime factor
#
# The prime factors of 13195 are 5, 7, 13 and 29.
# What is the largest prime factor of the number 600851475143 ?
#
# https://projecteuler.net/problem=3

'''
Having implemented various factorisation algorithms in Java for one of my university modules before, I didn't want to reimplement it all over again in Python.

Therefore, I decided to use primesfrom2to(n) from http://stackoverflow.com/a/3035188
and isprime(n) from https://www.daniweb.com/software-development/python/code/216880/check-if-a-number-is-a-prime-number-python
'''

import numpy

THE_NUMBER = 600851475143
def primesfrom2to(n):
    """ Input n>=6, Returns a array of primes, 2 <= p < n """
    sieve = numpy.ones(n/3 + (n%6==2), dtype=numpy.bool)
    for i in range(1,int((n**0.5)/3+1)):
        if sieve[i]:
            k=3*i+1|1
            sieve[       k*k/3     ::2*k] = False
            sieve[k*(k-2*(i&1)+4)/3::2*k] = False
    return numpy.r_[2,3,((3*numpy.nonzero(sieve)[0][1:]+1)|1)]

def isprime(n):
    '''check if integer n is a prime'''
    # make sure n is a positive integer
    n = abs(int(n))
    # 0 and 1 are not primes
    if n < 2:
        return False
    # 2 is the only even prime number
    if n == 2:
        return True
    # all other even numbers are not primes
    if not n & 1:
        return False
    # range starts with 3 and only needs to go up the squareroot of n
    # for all odd numbers
    for x in range(3, int(n**0.5)+1, 2):
        if n % x == 0:
            return False
    return True

factors = []
for i in primesfrom2to(10000000):
    if THE_NUMBER % i == 0:
        print("%d is a factor!" % i)
        factors.append(i)


print("%d seems to be the answer" % factors[-1])
