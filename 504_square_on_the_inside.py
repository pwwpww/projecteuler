# Problem 504
#
# Square on the Inside
#
# Let ABCD be a quadrilateral whose vertices are lattice points lying on the coordinate axes as follows:
# A(a, 0), B(0, b), C(âc, 0), D(0, âd), where 1 ≤ a, b, c, d ≤ m and a, b, c, d, m are integers.
# It can be shown that for m = 4 there are exactly 256 valid ways to construct ABCD. Of these 256 quadrilaterals, 42 of them <u>strictly</u> contain a square number of lattice points.
# How many quadrilaterals ABCD strictly contain a square number of lattice points for m = 100?
#
# https://projecteuler.net/problem=504
