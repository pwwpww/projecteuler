# Problem 421
#
# Prime factors of n15+1
#
# 
# Numbers of the form n15+1 are composite for every integer n &gt; 1.
# For positive integers n and m let s(n,m) be defined as the sum of the distinct prime factors of n15+1 not exceeding m.
# 
# 
# Find &amp;Sum; s(n,108) for 1 ≤ n ≤ 1011.
# 
#
# https://projecteuler.net/problem=421
