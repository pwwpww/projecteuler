# Problem 261
#
# Pivotal Square Sums
#
# Let us call a positive integer k a square-pivot, if there is a pair of integers m &gt; 0 and n ≥ k, such that the sum of the (m+1) consecutive squares up to k equals the sum of the m consecutive squares from (n+1) on:
# Some small square-pivots are
# 
# Find the sum of all distinct square-pivots ≤ 1010.
#
# https://projecteuler.net/problem=261
