# Problem 625
#
# Gcd sum
#
# 
$G(N)=\sum_{j=1}^N\sum_{i=1}^j \text{gcd}(i,j)$. <br>
You are given: $G(10)=122$.</br>
# 
Find $G(10^{11})$. Give your answer modulo 998244353

#
# https://projecteuler.net/problem=625
