# Problem 341
#
# Golomb's self-describing sequence
#
# The Golomb's self-describing sequence {G(n)} is the only nondecreasing sequence of natural numbers such that n appears exactly G(n) times in the sequence. The values of G(n) for the first few n are
# 
# You are given that G(103) = 86, G(106) = 6137.
# You are also given that ΣG(n3) = 153506976 for 1 ≤ n &lt; 103.
# Find ΣG(n3) for 1 ≤ n &lt; 106.
#
# https://projecteuler.net/problem=341
