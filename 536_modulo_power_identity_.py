# Problem 536
#
# Modulo power identity 
#
# 
Let S(n) be the sum of all positive integers m not exceeding n having the following property:<br>a m+4 ≡ a (mod m) for all integers a.
</br>
# 
The values of m ≤ 100 that satisfy this property are 1, 2, 3, 5 and 21, thus S(100) = 1+2+3+5+21 = 32.
You are given S(106) = 22868117.

# 
Find S(1012).

#
# https://projecteuler.net/problem=536
