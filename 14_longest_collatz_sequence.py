# Problem 14
#
# Longest Collatz sequence
#
# The following iterative sequence is defined for the set of positive integers:
# n → n/2 (n is even)
# n → 3n + 1 (n is odd)
# Using the rule above and starting with 13, we generate the following sequence:
# 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
# It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.
# Which starting number, under one million, produces the longest chain?
# NOTE: Once the chain starts the terms are allowed to go above one million.
#
# https://projecteuler.net/problem=14

sequence = []

longestsequence = 0
longeststartingnumber = 0

def nextinsequence(n):
    next = 0
    if n % 2 == 0:
        next = n // 2
    else:
        next = (n * 3) + 1
    return next

for i in range(1,1000001):
    if i % 1000 == 0:
        print(str(i) + "...")

    sequence = []
    sequence.append(i)
    while sequence[-1] != 1:
        sequence.append(nextinsequence(sequence[-1]))

    if len(sequence) > longestsequence:
        longestsequence = len(sequence)
        longeststartingnumber = i


print("Longest chain: " + str(longestsequence))
print("Starting number: " + str(longeststartingnumber))