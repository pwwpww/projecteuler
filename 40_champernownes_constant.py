# Problem 40
#
# Champernowne's constant
#
# An irrational decimal fraction is created by concatenating the positive integers:
# <p style="text-align:center;">0.12345678910<span style="color:#dd0000;font-size:14pt;">1</span>112131415161718192021...
# It can be seen that the 12th digit of the fractional part is 1.
# If dn represents the nth digit of the fractional part, find the value of the following expression.
# <p style="text-align:center;">d1 × d10 × d100 × d1000 × d10000 × d100000 × d1000000
#
# https://projecteuler.net/problem=40
