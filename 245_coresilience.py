# Problem 245
#
# Coresilience
#
# We shall call a fraction that cannot be cancelled down a resilient fraction. Furthermore we shall define the resilience of a denominator, R(d), to be the ratio of its proper fractions that are resilient; for example, R(12) = 4⁄11.
# Find the sum of all composite integers 1 &lt; n ≤ 2×1011, for which C(n) is a unit fraction (A fraction with numerator 1).
# 
#
# https://projecteuler.net/problem=245
