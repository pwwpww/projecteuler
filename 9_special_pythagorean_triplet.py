# Problem 9
#
# Special Pythagorean triplet
#
# A Pythagorean triplet is a set of three natural numbers, a < b < c, for which, a^2 + b^2 = c ^ 2
# For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
# There exists exactly one Pythagorean triplet for which a + b + c = 1000.Find the product abc.
#
# https://projecteuler.net/problem=9

flag = True
a = b = c = 0
while flag and a < 1000:
    a += 1
    print("a = " + str(a))
    b = 0
    while flag and b < 1000:
        b += 1
        #print("b = " + str(b))
        c = 0
        while flag and c < 1000:
            c += 1
            #print("c = " + str(c))
            if a < b < c and (a**2 + b**2) == (c ** 2):
                print("Pythagorean triplet! " + str(a) + " " + str(b) + " " + str(c))
                if (a + b + c) == 1000:
                    print(str(a) + " " + str(b) + " " + str(c))
                    flag = not flag