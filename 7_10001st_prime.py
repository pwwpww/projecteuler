# Problem 7
#
# 10001st prime
#
# By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
# What is the 10 001st prime number?
#
# https://projecteuler.net/problem=7

import numpy, math

def prime(upto):
    primes = numpy.arange(2, upto+1)
    isprime = numpy.ones(upto - 1, dtype=bool)
    for factor in primes [:int(math.sqrt(upto))]:
        if isprime[factor-2]: isprime[factor*2-2::factor]=0
    return primes[isprime]

primeslist = prime(1000000)
print(primeslist[10000])
print(len(primeslist))