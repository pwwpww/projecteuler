# Problem 58
#
# Spiral primes
#
# Starting with 1 and spiralling anticlockwise in the following way, a square spiral with side length 7 is formed.
# <p style="text-align:center;font-family:courier new;"><span style="color:#ff0000;font-family:courier new;">37</span> 36 35 34 33 32 <span style="color:#ff0000;font-family:courier new;">31</span>
# 38 <span style="color:#ff0000;font-family:courier new;">17</span> 16 15 14 <span style="color:#ff0000;font-family:courier new;">13</span> 30
# 39 18 <span style="color:#ff0000;font-family:courier new;"> 5</span>  4 <span style="color:#ff0000;font-family:courier new;"> 3</span> 12 29
# 40 19  6  1  2 11 28
# 41 20 <span style="color:#ff0000;font-family:courier new;"> 7</span>  8  9 10 27
# 42 21 22 23 24 25 26
<span style="color:#ff0000;font-family:courier new;">43</span> 44 45 46 47 48 49
# It is interesting to note that the odd squares lie along the bottom right diagonal, but what is more interesting is that 8 out of the 13 numbers lying along both diagonals are prime; that is, a ratio of 8/13 ≈ 62%.
# If one complete new layer is wrapped around the spiral above, a square spiral with side length 9 will be formed. If this process is continued, what is the side length of the square spiral for which the ratio of primes along both diagonals first falls below 10%?
#
# https://projecteuler.net/problem=58
