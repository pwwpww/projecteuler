# Problem 5
#
# Smallest multiple
#
# 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
#
# What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
#
# https://projecteuler.net/problem=5

#bruteforce method: the number must be a multiple of 2520

def bruteforce(i):
    while True:
        i += 2520

        count = 0

        for j in range(2,21):

            if i % j == 0:
                count += 1

            if count > 18:
                answer = "answer: " + str(i)
                return answer

print(bruteforce(2520))