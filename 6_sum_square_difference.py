# Problem 6
#
# Sum square difference
#
# The sum of the squares of the first ten natural numbers is,
# The square of the sum of the first ten natural numbers is,
# Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.
# Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
#
# https://projecteuler.net/problem=6

HIGH = 100

sum_squares = 0
sum_numbers = 0
for i in range(1, HIGH + 1):
    sum_numbers = sum_numbers + i
    sum_squares = sum_squares + i**2


print(str(sum_numbers**2) + " - " + str(sum_squares) + " = " + str(sum_numbers**2 - sum_squares))