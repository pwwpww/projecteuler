# Problem 454
#
# Diophantine reciprocals III
#
# In the following equation x, y, and n are positive integers.
# For a limit L we define F(L) as the number of solutions which satisfy x &lt; y ≤ L.
# We can verify that F(15) = 4 and F(1000) = 1069.
# Find F(1012).
#
# https://projecteuler.net/problem=454
