# Problem 211
#
# Divisor Square Sum
#
# For a positive integer n, let σ2(n) be the sum of the squares of its divisors. For example,
# Find the sum of all n, 0 &lt; n &lt; 64,000,000 such that σ2(n) is a perfect square.
#
# https://projecteuler.net/problem=211
