# Problem 551
#
# Sum of digits sequence
#
# Let a0, a1, a2, ... be an integer sequence defined by:
# The sequence starts with 1, 1, 2, 4, 8, 16, 23, 28, 38, 49, ...<br>
You are given a106 = 31054319.</br>
# Find a1015.
#
# https://projecteuler.net/problem=551
