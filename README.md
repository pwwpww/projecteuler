# projecteuler
My solutions to Project Euler tasks, all written in Python 3.

I've also written a little hacky [scraper](project_euler_scraper.py) that downloads all of the problems into separate .py files in the current directory, with the problem description and a link to the problem included as a comment.
It's not an ideal solution, as some problems include some fancy HTML formatting that doesn't look that great in the comment form, others include images.
Still, it's convenient enough for most problems!

Before the initial commit I have managed to solve the first 17 problems.

[Following this advice](https://projecteuler.chat/viewtopic.php?p=46661&sid=36286c2c5d8cbd89892e5da4203526f8#p46661), I'll stop sharing my answers after completing #100.