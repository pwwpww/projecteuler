# Problem 10
#
# Summation of primes
#
# The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
# Find the sum of all the primes below two million.
#
# https://projecteuler.net/problem=10

import numpy, math

def prime(upto):
    primes = numpy.arange(2, upto+1)
    isprime = numpy.ones(upto - 1, dtype=bool)
    for factor in primes [:int(math.sqrt(upto))]:
        if isprime[factor-2]: isprime[factor*2-2::factor]=0
    return primes[isprime]

primeslist = prime(2000000)
print(numpy.sum(primeslist))