# Problem 484
#
# Arithmetic Derivative
#
# The <strong>arithmetic derivative</strong> is defined by
# For example, 20' = 24
# Find ∑ <strong>gcd</strong>(k,k') for 1 &lt; k ≤ 5·1015
# Note: <strong>gcd</strong>(x,y) denotes the greatest common divisor of x and y.
#
# https://projecteuler.net/problem=484
