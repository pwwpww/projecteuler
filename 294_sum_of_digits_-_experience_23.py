# Problem 294
#
# Sum of digits - experience #23
#
# 
# For a positive integer k, define d(k) as the sum of the digits of k in its usual decimal representation.
# Thus d(42) = 4+2 = 6.
# 
# 
# For a positive integer n, define S(n) as the number of positive integers k n with the following properties :
# 
# 
# Find S(1112) and give your answer mod 109.
# 
#
# https://projecteuler.net/problem=294
