# Problem 17
#
# Number letter counts
#
# If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
# If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used? 
# NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out numbers is in compliance with British usage.
#
# https://projecteuler.net/problem=17

numbers = {1: 'one', 2: 'two', 3: 'three', 4: 'four', 5: 'five', 6: 'six', 7: 'seven', 8: 'eight', 9: 'nine', 10: 'ten',
           11: 'eleven', 12: 'twelve', 13: 'thirteen', 15: 'fifteen', 18: 'eighteen',
           20: 'twenty', 30: 'thirty', 40: 'forty', 50: 'fifty', 60: 'sixty', 70: 'seventy', 80: 'eighty', 90: 'ninety',
           100: 'hundred'}

def getnumbername(n):
    number = str(n)
    newnumber = ''

    if len(number) < 2 or len(number) < 3 and n in numbers: #1 to 13 and 15, 18 and 20... 90
        newnumber = numbers[n]
    elif len(number) < 3 and n < 20:    #14, 16, 17, 19
        newnumber = numbers[int(number[-1])] + 'teen'
    elif len(number) < 3 and n > 20:    #21+ excluding 20... 90
        newnumber = numbers[int(number[:1] + str(0))] + numbers[int(number[-1])]
    elif len(number) < 4:   #100 to 999
        newnumber = numbers[int(number[:1])] + numbers[100] #100... 900
        if n % 100 != 0:
            newnumber = newnumber + 'and' + getnumbername(int(number[1:]))
    elif n == 1000:
        newnumber = 'onethousand'



    return newnumber

count = 0
for i in range(1, 1000+1):
    print(getnumbername(i))
    count = count + len(getnumbername(i))

print(count)