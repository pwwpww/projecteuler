# Problem 108
#
# Diophantine reciprocals I
#
# In the following equation x, y, and n are positive integers.
# For n = 4 there are exactly three distinct solutions:
# What is the least value of n for which the number of distinct solutions exceeds one-thousand?
# <p class="note">NOTE: This problem is an easier version of <a href="problem=110">Problem 110</a>; it is strongly advised that you solve this one first.
#
# https://projecteuler.net/problem=108
