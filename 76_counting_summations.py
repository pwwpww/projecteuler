# Problem 76
#
# Counting summations
#
# It is possible to write five as a sum in exactly six different ways:
# <p style="margin-left:50px;">4 + 1
3 + 2
3 + 1 + 1
2 + 2 + 1
2 + 1 + 1 + 1
1 + 1 + 1 + 1 + 1
# How many different ways can one hundred be written as a sum of at least two positive integers?
#
# https://projecteuler.net/problem=76
